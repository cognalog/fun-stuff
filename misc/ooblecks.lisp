;returns a list of the powers of m that don't exceed n
(defun powers (n m)
  (reverse (loop for x from 0
       until (> (expt m x) n)
       collect (expt m x))))

;returns a list of the factors for the respective powers (higehst first)
(defun factors (n ls)
  (cond
    ((null ls) ls)
    (T (cons (floor (/ n (car ls))) 
	     (factors (mod n (car ls)) (cdr ls))))))

;does the heavy lifting
(defun superAug (m ls)
  (cond
    ((null ls) ls)
    ((null (cdr ls)) 1)
    (T (loop for i from 0 to (car ls)
	    collect (superAug m (cons (+ (cadr ls) (* i m))
				      (cddr ls)))))))

;returns the sum of all the numbers (nested or not) in ls
(defun sum (ls)
  (cond
    ((null ls) 0)
    ((not (listp ls)) ls)
    ((listp (car ls)) (+ (sum (car ls)) (sum (cdr ls))))
    (T (+ (car ls) (sum (cdr ls))))))

;driver function
(defun ooblecks (n m)
  (sum (superAug m (factors n (powers n m)))))