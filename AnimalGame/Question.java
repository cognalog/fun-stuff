import java.io.*;
import java.util.*;

/**
 * Class defining the binary tree used in the animal game, as well as critical mechanics such as loading and input-driven play
 * @author Ty Hinderson
 */ 
public class Question {
 
 private String query;
 private Question right;
 private Question left;
 
 public Question(String s){
  query = s;
 }
 
 /**
  * converts the tree from file-based form to a memory-based tree
  * @param line the string that will form the current node
  * @param node the current node being (re)created
  * @throws IOException
  */
 public void load(String line, BufferedReader reader, Stack<Question> qStack) throws IOException{
  if(line.indexOf('~') >= 0) //get rid of tilde marker so it doesn't appear to player
    query = line.substring(0, line.indexOf('~'));
  else
    query = line;
  //System.out.println(query);
  String q = reader.readLine();
  if(q != null) //make sure there are more lines to read
  {
   if(line.indexOf('~') >= 0){
    setLeft(new Question(""));
    setRight(new Question(""));
    qStack.add(this);
    getLeft().load(q, reader, qStack);
   }
   else
    qStack.pop().getRight().load(q, reader, qStack);
  }
  //else
   //System.out.println("done loading");
  reader.close();
 }
 /**
  * Asks one question and reacts to the answer.  If an animal guess is incorrect, a new animal and Question are created from input to accommodate the same outcome in the future
  * @param stdin the buffered reader receiving user input
  * @param stdout the normal printstream
  * @param stderr the error printstream
  * @throws IOException just in case.
  */ 
 public void play(BufferedReader stdin, PrintStream stdout, PrintStream stderr) throws IOException{
  if(hasKids())
   System.out.println(getQuery());
  else
   stdout.println("Is it a(n) " + getQuery());
  String yn = "";
  try {
   yn = stdin.readLine();
  } catch (IOException e) {
   stderr.println("Input fail");
  }
  while(!yn.equalsIgnoreCase("yes") && !yn.equalsIgnoreCase("no")){//another yes/no constraint...I really should make a method for this.
     stdout.println("please answer \"yes\" or \"no\".");
      yn = stdin.readLine(); 
    }
  if(yn.equalsIgnoreCase("yes")){
   if(hasKids())
    getLeft().play(stdin, stdout, stderr);
   else{
    stdout.println("Awesome."); //computer wins
    Animal.save();
   }
  }
  else if(yn.equalsIgnoreCase("no")){
   if(hasKids())
    getRight().play(stdin, stdout, stderr);
   else{  //user wins, build a new node preceded by a new y/n question
    stdout.println("You win this round; what was your animal?");
    Question newAnimal = new Question(stdin.readLine());
    stdout.println("Provide a question that MOST distinguishes a(n) " + newAnimal.getQuery() + " from a(n) " + getQuery() + ":");
    String newQuestion = stdin.readLine();
    stdout.println("What is the answer for " + newAnimal.getQuery() + "?");
    String lr = stdin.readLine();
    while(!lr.equalsIgnoreCase("yes") && !lr.equalsIgnoreCase("no")){
     stdout.println("please answer \"yes\" or \"no\".");
      lr = stdin.readLine(); 
    }
    stdout.println("Congrats again on winning The Animal Game, and thank you for your contribution!");
    Question copy = copy();
    setQuery(newQuestion);
    if(lr.equalsIgnoreCase("yes")){
     setLeft(newAnimal);
     setRight(copy);
    }
    if(lr.equalsIgnoreCase("no")){
     setLeft(copy);
     setRight(newAnimal);
    }
    String s = "";
    Animal.save();
    stdout.println(s);
   }
  }
 }
 
 /**
  * fetches the query value
  * @return query said query value
  */
 public String getQuery(){
  return query;
 }
 
 /**
  * fetches the Question contained within the right branch node
  * @return right said Question
  */
 public Question getRight(){
  return right;
 }
 
 /**
  * fetches the Question contained within the left branch node
  * @return left said Question
  */
 public Question getLeft(){
  return left;
 }
 
 /**
  * sets the query value to s
  * @param s the value to which the query will be changed
  */
 public void setQuery(String s){
  query = s;
 }
 
 /**
  * sets the right Question's query value to s
  * @param s the value to which the query will be changed
  */
 public void setRight(Question q){
  right = q;
 }
 
 /**
  * sets the left Question's query value to s
  * @param s the value to which the query will be changed
  */
 public void setLeft(Question q){
  left = q;
 }
 
 /**
  * determines whether a node has child nodes, and therefore whether it is an internal (question) or external (animal) node
  */
 public boolean hasKids(){
  return !(right == null || left == null);
 }
 
 /**
  * renders a copy of this Question
  * @return q the aforementioned copy
  */
 public Question copy(){
  Question q = new Question(query);
  q.setRight(right);
  q.setLeft(left);
  return q;
 }
}
