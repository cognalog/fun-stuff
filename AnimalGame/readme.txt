I hope you enjoy perusing and playing the fruits of my labor as much as I enjoyed laboring for them.

In the meantime, a few things you should know:


-other comments are included throughout the code intermittently so that things hopefully make sense.
-In the load method I use a Stack briefly to help with traversing the tree backwards so that the correct nodes would be attached to the correct upper nodes. The stack is empty whenever loading isn't taking place.
-I included a file called "dump" as well as Knowledge.  The latter has the one starting animal, the former has a tree with some animals already, in case you wanted a little head start.
-if you're not malicious, things should go quite nicely.  If you ARE, then enjoy the unpleasant strings of errors that will likely come your way.
-that is all.