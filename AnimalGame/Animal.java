import java.io.*;
import java.util.*;

/**
 * Driver class for the Animal Game.
 * @author Ty Hinderson
 */ 
public class Animal {

 private static PrintStream stdout = System.out;
 private static BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
 private static BufferedReader reader;
 private static PrintStream stderr = System.err;
 private static Stack<Question> qStack = new Stack<Question>();
 private static Question root = new Question("");
 private static boolean done = false;
 private static String saveString;
 
 
 /**
  * @param args
  * @throws IOException 
  */
 public static void main(String[] args) throws IOException {
  while(done != true){
  stdout.println("Would you like to play the Animal Game?");
  String answer = stdin.readLine();
  
  while(!answer.equalsIgnoreCase("no") && !answer.equalsIgnoreCase("yes")){ //make sure nothing but "yes" or "no" gets through
   stdout.println("please answer \"yes\" or \"no\".");
   answer = stdin.readLine();
  }
  
  if(answer.equalsIgnoreCase("no")){ //if user has played a few times and had enough, here's the exit
   stdout.println("Have a nice day!");
   done = true;
  }
  else if(answer.equalsIgnoreCase("yes")){
  //play the game
  try {
   reader = new BufferedReader(new FileReader(/*for unix use: args[0]*/"Knowledge"));
  } catch (FileNotFoundException e) {
   stdout.println("can't find the file");
  }
   saveString = "";
   String first = reader.readLine();
   root.load(first, reader, qStack);
   root.play(stdin, stdout, stderr);
   
   //save the game after playing a round
   if(root != null){
    BufferedWriter writer = new BufferedWriter(new FileWriter(/*for unix use: args[0]*/"Knowledge"));
    writer.write(saveString);
    writer.close();
    //checkTree(root, root);
    //stdout.println(saveString);
   }
  }  else{
   stdout.println("I do not understand.  Please answer with \"yes\" or \"no\".");
  }
  }
 }
 
 /**
  *Prints out the tree in a format similar to the file-based format, but with dashes next to each node indicating the previous node.
  * @param q the current node being processed
  * @param p the node from which q descended
  */
 public static void checkTree(Question q, Question p){
   stdout.print(q.getQuery());
   stdout.println("-"+p.getQuery());
   if(q.hasKids()){
     checkTree(q.getLeft(), q);
     checkTree(q.getRight(), q);
   }
 }
 
 /**
  * A public version of save that can be used by other classes without compromising security (root remains private)
  * @throws IOException in case something goes HORRIBLY wrong.  you never know.
  */ 
 public static void save() throws IOException{
   save(root);
 }
 
 /**
  * Converts the tree into a file-based form
  * @param node the node currently being saved
  * @throws IOException 
  */
 private static void save(Question node) throws IOException{
  if(!node.hasKids()){
   saveString += node.getQuery() + "\n";
   //stdout.println(node.getQuery());
  }
  else{
   saveString += node.getQuery() + "~\n";
   //System.out.println(node.getQuery());
   save(node.getLeft());
   save(node.getRight());
  }
  }
}
