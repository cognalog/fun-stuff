import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Kim here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Kim  extends Actor
{
    public GreenfootImage normPic;
    public GreenfootImage lolPic;
    private int speed;
    public boolean done;
    /**
     * Act - do whatever the Kim wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public Kim()
    {
        normPic = new GreenfootImage("kim_face.gif");
        lolPic = new GreenfootImage("kim_crazy.gif");
        speed = 2;
    }
    public void addedToWorld(World world)
    {
        setImage(normPic);
    }
    public void act() 
    {
        if(getY() > 25)
        {
            setLocation(getX(), getY() - speed);
        }
        if(getY() <= 25 && getX() > 25)
        {
            setLocation(getX() - speed, getY());
        }
    }    
}
