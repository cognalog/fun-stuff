import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class DKWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DKWorld  extends World
{
    private static final int totalCells = 120;
    private static final int cellSize = 10;
    private static boolean addStud = true;
    private static boolean addText = true;
    public static Kim kim = new Kim();
    public static Stud stud = new Stud();
    public static Text txt = new Text();
    private static int txtTimer = 0;
    
    //platforming
    private static Floor[] floors = new Floor[4];
    private static Chip[] chips = new Chip[5];
    
    //books
    private static int bookTimer = 75;
    private static Book[] books = new Book[5];
    /**
     * Constructor for objects of class DKWorld.
     * 
     */
    public DKWorld()
    {    
        // Create a new world with 20x20 cells with a cell size of 10x10 pixels.
        super(250, 250, 2);
        for(int i = 0; i < floors.length; i++)
        {
            floors[i] = new Floor();
        }
        for(int i = 0; i < chips.length; i++)
        {
            chips[i] = new Chip();
            if(i % 2 == 0)
                chips[i].setImage(chips[i].up);
            else
                chips[i].setImage(chips[i].down);
        }
        for(int i = 0; i < books.length; i++)
        {
            books[i] = new Book();
        }
        //setBackground("integrated-circuit.gif");
    } 
    
    public void started()
    {
        GreenfootImage bg = getBackground();
        bg.setColor(new Color(51, 204, 0));
        bg.fill();
        for(int i = 0; i < floors.length; i++)
        {
            addObject(floors[i], 125, 240 - i * 65);
        }
        for(int i = 0; i < chips.length; i++)
        {
            if(i == 0)
                addObject(chips[i], 225, 225);
            else if(i % 2 == 0)
                addObject(chips[i], 225, chips[i-1].getY());
            else
                addObject(chips[i], 25, chips[i-1].getY() - 65);
        }
        addObject(kim, 125, 125);
    }

    public void stopped()
    {
        /*
        for(Object a : getObjects(null))
        {
            removeObject((Actor)a);
        }
        */
    }
    
    public void act()
    {
        if(kim.getX() <= 25 && kim.getY() <= 25 && addText)
        {
            addObject(txt, 125, 10);
            addText = false;
        }
        if(txtTimer > 120 && txt.getImage() == txt.one)
        {
            kim.setImage(kim.lolPic);
            txt.setImage(txt.two);   
        }
        else
        {
            txtTimer++;
        }
        if(txt.getImage() == txt.two)
        {
            addObject(stud, 25, 225);
            addStud = false;
        }
        
        // up-down transportation
        if(stud.exists)
        {
            for(int i = 0; i < chips.length; i++)
            {
                if(chips[i].detect(Stud.class))
                {
                    if(i == 4)
                    {
                        stud.setLocation(225, stud.getY() - 65);
                        stud.onFloor -= 65;
                    }
                    else if(i % 2 == 0)
                    {
                        stud.setLocation(50, stud.getY() - 65);
                        stud.onFloor -= 65;
                    }
                    else
                    {
                        stud.setLocation(200, stud.getY() + 65);
                        stud.onFloor += 65;
                    }
                }
            }
            for(Book b : books)
            {
                if(!b.exists)
                {
                    if(bookTimer > 75)
                    {
                        addObject(b, 50, 30);
                        bookTimer = Greenfoot.getRandomNumber(50);
                    }
                }
                else 
                {
                    if(b.getX() <= 10)
                    {
                        if(b.getY() > 200)
                        {
                            b.exists = false;
                            removeObject(b);
                            continue;
                        }
                        else
                        {
                            b.setLocation(225, b.getY() + 65);
                        }
                    }
                    else if(b.getX() >= 240 && b.getY() <= 30)
                    {
                        b.setLocation(225, b.getY() + 65);
                    }
                    
                    //test for stud hits
                    if(b.detect(Stud.class))
                    {
                        gameOver();
                    }
                }
            }
            bookTimer++;
        }
    }
    
    public void gameOver()
    {
        for(int i = 0; i < books.length; i++)
        {
            books[i].stop = true;
        }
        
        stud.stop = true;
        txt.setImage(txt.three);
        txt.setLocation(130, 10);
        
        Greenfoot.delay(30);
        txt.setImage(txt.four);
        txt.setLocation(125, 125);
        Greenfoot.stop();
    }
    
    public void drawBackground()
    {
        GreenfootImage bg = getBackground();
        bg.setColor(Color.BLACK);
        for(int i = 0; i < totalCells; i++)
        {
            bg.drawLine(i * cellSize, 0, i * cellSize, totalCells * cellSize);
            bg.drawLine(0, i * cellSize, totalCells * cellSize, i * cellSize);
        }
    }
}
