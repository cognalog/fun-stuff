import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Text here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Text  extends Actor
{
    GreenfootImage one;
    GreenfootImage two;
    GreenfootImage three;
    GreenfootImage four;
    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Text()
    {
        one = new GreenfootImage("txt.png");
        two = new GreenfootImage("txt2.png");
        three = new GreenfootImage("txt3.png");
        four = new GreenfootImage("txt4.png");
    }
    
    public void addedToWorld(World world)
    {
        setImage(one);
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
