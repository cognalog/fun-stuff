import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Book here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Book  extends Actor
{
    private int speed;
    private GreenfootImage bPic;
    public boolean exists;
    public boolean stop;
    /**
     * Act - do whatever the Book wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public Book()
    {
        speed = Greenfoot.getRandomNumber(3) + 2;
        bPic = new GreenfootImage("book.png");
        exists = false;
        stop = false;
    }
    
    public void addedToWorld(World world)
    {
        setImage("book.png");
        exists = true;
    }
    
    public void rollRight()
    {
        setLocation(getX() + speed, getY());
        int rot = getRotation() + 10;
        if(rot == 360)
            rot = 0;
        setRotation(rot);
    }
    
    public void rollLeft()
    {
        setLocation(getX() - speed, getY());
        int rot = getRotation() - 10;
        if(rot == 360)
            rot = 0;
        setRotation(rot);
    }
    
    public boolean detect(java.lang.Class cls)
    {
        return !getObjectsInRange(15, cls).isEmpty();
    }
    
    public void act() 
    {
        if(!stop)
        {
            if(getY() <= 30)
            {
                rollRight();
            }
            else
            {
                rollLeft();
            }
        }
    }    
}
