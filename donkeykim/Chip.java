import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Chip here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Chip  extends Actor
{
    public GreenfootImage up;
    public GreenfootImage down;
    private boolean goingUp;
    private int height;
    /**
     * Act - do whatever the Chip wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public Chip()
    {
        up = new GreenfootImage("upChip.png");
        down = new GreenfootImage("downChip.png");
        goingUp = true;
    }
    
    public void addedToWorld(World world)
    {
        height = getY();
    }
    
    public boolean detect(java.lang.Class cls)
    {
        return !getObjectsInRange(5, cls).isEmpty();
    }
    public void act() 
    {
        if(goingUp)
        {
            if(getY() > height - 5)
                setLocation(getX(), getY() - 1);
            else
                goingUp = false;
        }
        else
        {
            if(getY() < height)
                setLocation(getX(), getY() + 1);
            else
                goingUp = true;
        }
    }    
}
