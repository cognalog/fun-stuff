import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Stud here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Stud  extends Actor
{
    private int speed; 
    public boolean airborne;
    private final int hops = -10;
    private int jumpV;
    private int gravity = 2;
    public boolean exists;
    public boolean stop;
    
    //anims
    private GreenfootImage run1 = new GreenfootImage("run1.png");
    private GreenfootImage run2 = new GreenfootImage("run2.png");
    private GreenfootImage run3 = new GreenfootImage("run3.png");
    private GreenfootImage run4 = new GreenfootImage("run4.png");
    private GreenfootImage run1_left = new GreenfootImage("run1_left.png");
    private GreenfootImage run2_left = new GreenfootImage("run2_left.png");
    private GreenfootImage run3_left = new GreenfootImage("run3_left.png");
    private GreenfootImage run4_left = new GreenfootImage("run4_left.png");
    private GreenfootImage jump1 = new GreenfootImage("jump1.png");
    private GreenfootImage jump1_left = new GreenfootImage("jump1_left.png");
    //ground levels
    public int onFloor;
    /**
     * Act - do whatever the Stud wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Stud()
    {
        speed = 3;
        airborne = false;
        jumpV = 0;
        onFloor = 225;
        exists = false;
        stop = false;
    }
    
    public void addedToWorld(World world)
    {
        exists = true;
    }
    
    private boolean active()
    {
        return Greenfoot.isKeyDown("right") || Greenfoot.isKeyDown("left") 
        || Greenfoot.isKeyDown("up");

    }
    
    public void moveRight()
    {
        setLocation(getX() + speed, getY());
            if(!airborne)
            {
                if(getImage() == run1)
                    setImage(run2);
                else if(getImage() == run2)
                    setImage(run3);
                else if(getImage() == run3)
                    setImage(run4);
                else
                    setImage(run1);
            }
            else
                setImage(jump1);
    }
    
    public void moveLeft()
    {
        setLocation(getX() - speed, getY());
            if(!airborne)
            {
                if(getImage() == run1_left)
                    setImage(run2_left);
                else if(getImage() == run2_left)
                    setImage(run3_left);
                else if(getImage() == run3_left)
                    setImage(run4_left);
                else
                    setImage(run1_left);
            }
            else
                setImage(jump1_left);
    }
    
    public void act() 
    {
        if(!stop)
        {
            if(Greenfoot.isKeyDown("right"))
            {
                moveRight();
            }
            if(Greenfoot.isKeyDown("left"))
            {
                moveLeft();
            }
            if(Greenfoot.isKeyDown("up") && !airborne)
            {
                jumpV = hops;
                airborne = true;
            }
        
            if(!active())
            {
                setImage("idle1.png");
                if(airborne)
                    setImage("jump1.png");
            }
        
            if(airborne)
            {
                setLocation(getX(), getY() + jumpV);
                jumpV += gravity;
                if(getY() >= onFloor)
                {
                    airborne = false;
                }
            }
        }
    }    
}
